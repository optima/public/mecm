######################
### MCM Client AIO ###
### v1.0.0         ###
### L.L            ###
######################

## VARIABLES
$ComputerName = $Env:COMPUTERNAME.ToLower()
$SuffixAD = ".ad.u-ga.fr"
$FullName = $ComputerName + "" + $SuffixAD
$CNFullName = "CN=" + "" + $FullName
$CertStoreLocation = "Cert:\LocalMachine\My"
$PathOutputFile = "C:\Users\Public\CertEnroll.txt"

## Remove PKI Certificat if it exists
Remove-Item -Path $PathOutputFile -Force -ErrorAction SilentlyContinue
If (Get-ChildItem -Path "Cert:\LocalMachine\My" | Where-Object {$_.Subject -eq $CNFullName}) {
    Write-Output "Le certificat PKI est installé. Suppression en cours..."
    Get-ChildItem $CertStoreLocation | Where-Object { $_.Subject -match $FullName } | Remove-Item
}
Else {
    Write-Output "Le certificat PKI n'est pas installé."
}

## Install new PKI certificat
Write-Output "Installation du certificat PKI..."
Get-Certificate -Template "MCM-Client" -CertStoreLocation $CertStoreLocation -Verbose | fl * | Out-File $PathOutputFile

## Download the MCM Client from Gitlab
$SourceUrl = "https://gricad-gitlab.univ-grenoble-alpes.fr/optima/public/mecm/-/raw/main/Client/ccmsetup.exe?inline=false"
$DestinationPath = "C:\Users\Public\ccmsetup.exe"
$ProgressPreference = 'SilentlyContinue'
Invoke-WebRequest -Uri $SourceUrl -OutFile $DestinationPath

## Define parameters
$CCMSetupPath = "C:\Users\Public\ccmsetup.exe"
$MPServer = "https://MCM-MGT.u-ga.fr"
$SMSSITECODE = "UGA"
$SMSMP = "https://MCM-MGT.u-ga.fr"
$FSP = "MCM-MGT.u-ga.fr"

## Add registry key
Write-Output "Installation de la clé de registre..."
New-Item -Path "HKLM:\SOFTWARE\Microsoft\SMS\Mobile Client" -Force
New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\SMS\Mobile Client" -Name "AssignedSiteCode" -Value "UGA" -PropertyType String -Force

## Check if CCMSetup.exe exists
if (Test-Path $CCMSetupPath) {
    try {
        # Build the argument list
        $Arguments = @(
            "/mp:$MPServer"
            "/SMSSITECODE=$SMSSITECODE"
            "/SMSMP=$SMSMP"
            "/FSP:$FSP"
            "/UsePKICert"
            "/skipprereq"
        )

        # Start the installation process
        Write-Output "Démarrage de l'installation du client MCM..."
        $Process = Start-Process -FilePath $CCMSetupPath -ArgumentList "/mp:https://MCM-MGT.u-ga.fr SMSSITECODE=UGA SMSMP=https://MCM-MGT.u-ga.fr FSP=MCM-MGT.u-ga.fr /UsePKICert" -Wait -PassThru -NoNewWindow

        # Check the exit code
        if ($Process.ExitCode -eq 0) {
            Write-Output "L'installation du client MCM s'est terminée avec succès"
        }
        else {
            Write-Output "Erreur lors de l'installation du client MCM : $($Process.ExitCode)"
        }

        # Wait for the SMS Agent Host service to start
        $Timeout = 300 # 5 minutes timeout
        $Timer = [Diagnostics.Stopwatch]::StartNew()
        
        Write-Output "En attente du démarrage du service CCMExec..."
        while ($Timer.Elapsed.TotalSeconds -lt $Timeout) {
            $Service = Get-Service -Name "CCMExec" -ErrorAction SilentlyContinue
            if ($Service.Status -eq "Running") {
                Write-Output "Le service CCMExec est en cours d'exécution"
                break
            }
            Start-Sleep -Seconds 10
        }

        if ($Timer.Elapsed.TotalSeconds -ge $Timeout) {
            Write-Output "Délai d'attente dépassé pour le démarrage du service CCMExec"
        }

    }
    catch {
        Write-Output "Une erreur s'est produite lors de l'installation : $_"
    }
}
else {
    Write-Output "CCMSetup.exe est introuvable dans l'emplacement spécifié : $CCMSetupPath"
}

## Display client installation status
$ClientVersion = Get-WmiObject -Namespace "root\ccm" -Class "SMS_Client" -ErrorAction SilentlyContinue
if ($ClientVersion) {
    Write-Output "Version du client MCM : $($ClientVersion.ClientVersion)"
}
else {
    Write-Output "Impossible de récupérer la version du client MCM"
}
